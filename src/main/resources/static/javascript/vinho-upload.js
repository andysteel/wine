var wine = wine || {};

wine.uploadFoto = (function(){
	
	function uploadFoto(){
		this.uploadDrop = $('#upload-drop');
		this.containerFoto = $('.js-container-foto')
	}
	
	uploadFoto.prototype.iniciar = function(){
		
		var settings = {
				type:'json',
				filelimit:1,
				allow:'*.(jpg|jpeg|png)',
				action:'/fotos/'+this.uploadDrop.data('codigo'),
				complete: onUploadCompleto.bind(this),
				beforeSend: adicionarCSRFToken
		};
			
			UIkit.uploadSelect($('#upload-select'),settings);
			UIkit.uploadDrop(this.uploadDrop,settings);
		
	}
	
	function adicionarCSRFToken(xhr){
		var header = $('input[name=_csrf_header]').val();
		var token = $('input[name=_csrf]').val();
		xhr.setRequestHeader(header, token);
	}
	
	function onUploadCompleto(foto){
		this.uploadDrop.addClass('hidden');
		this.containerFoto.prepend('<img src="'+ foto.url+'" class="img-responsive" style="margin: auto;" />');
	}
	
	return uploadFoto;
})();

$(function(){
	
	var uploadFoto = new wine.uploadFoto();
	uploadFoto.iniciar();
	
})
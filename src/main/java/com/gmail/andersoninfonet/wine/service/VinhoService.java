package com.gmail.andersoninfonet.wine.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.gmail.andersoninfonet.wine.model.Vinho;
import com.gmail.andersoninfonet.wine.repository.VinhoRepository;
import com.gmail.andersoninfonet.wine.storage.FotoStorage;

@Service
public class VinhoService {

	@Autowired
	private VinhoRepository repository;
	@Autowired
	private FotoStorage storage;
	
	public void salvar(Vinho vinho){
		repository.save(vinho);
	}
	
	public List<Vinho> listar(){
		return repository.findAll();
	}
	
	public Vinho buscar(Long codigo){
		return repository.findOne(codigo);
	}
	
	public String salvarFoto(Long codigo, MultipartFile foto){
		Vinho vinho = repository.findOne(codigo);
		String nomeFoto = storage.salvar(foto);
		vinho.setFoto(nomeFoto);
		repository.save(vinho);
		
		return storage.getUrl(nomeFoto);
	}
}

package com.gmail.andersoninfonet.wine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gmail.andersoninfonet.wine.dto.Foto;
import com.gmail.andersoninfonet.wine.service.VinhoService;
import com.gmail.andersoninfonet.wine.storage.FotoReader;

@RestController
@RequestMapping("/fotos")
public class UploadController {
	
	@Autowired
	private VinhoService service;
	@Autowired
	private FotoReader reader;

	@RequestMapping(value="/{codigo}",method=RequestMethod.POST)
	public Foto upload(@PathVariable Long codigo,@RequestParam("files[]")MultipartFile[] files){
		String url = service.salvarFoto(codigo, files[0]);
		return new Foto(url);
	}
	
	@RequestMapping("/{nome:.*}")
	public byte[] recuperar(@PathVariable String nome){
		return reader.recuperar(nome);
	}
}

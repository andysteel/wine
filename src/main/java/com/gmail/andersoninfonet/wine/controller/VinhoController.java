package com.gmail.andersoninfonet.wine.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gmail.andersoninfonet.wine.model.TipoVinho;
import com.gmail.andersoninfonet.wine.model.Vinho;
import com.gmail.andersoninfonet.wine.service.VinhoService;
import com.gmail.andersoninfonet.wine.storage.FotoStorage;

@Controller
@RequestMapping("/vinhos")
public class VinhoController {

	@Autowired
	private VinhoService service;
	
	@Autowired
	private FotoStorage storage;
	
	@RequestMapping
	public ModelAndView pesquisa() {
		ModelAndView mv = new ModelAndView("/vinho/ListagemVinhos");
		return mv.addObject("vinhos", service.listar());
	}
	
	@RequestMapping("/novo")
	public ModelAndView novo(Vinho vinho) {
		ModelAndView mv = new ModelAndView("/vinho/CadastroVinho");
		mv.addObject("tipos", TipoVinho.values());
		return mv;
	}
	
	@RequestMapping(value="/novo",method=RequestMethod.POST)
	public ModelAndView salvar(@Valid Vinho vinho, BindingResult result, RedirectAttributes attributes){
		if(result.hasErrors()){
			return novo(vinho);
		}
		service.salvar(vinho);
		attributes.addFlashAttribute("mensagem", "Vinho salvo com sucesso !");
		return new ModelAndView("redirect:/vinhos/novo");
	}
	
	@RequestMapping("/{codigo}")
	public ModelAndView visualizar(@PathVariable Long codigo){
		ModelAndView mv = new ModelAndView("/vinho/VisualizacaoVinho");
		Vinho vinho = service.buscar(codigo);
		
		if(vinho.temFoto()){
			vinho.setUrl(storage.getUrl(vinho.getFoto()));
		}
		mv.addObject("vinho", vinho);
		return mv;
	}
}

package com.gmail.andersoninfonet.wine.storage;

public interface FotoReader {

	public byte[] recuperar(String nome);
}

package com.gmail.andersoninfonet.wine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gmail.andersoninfonet.wine.model.Vinho;

public interface VinhoRepository extends JpaRepository<Vinho, Long>{

}
